List and description of files 

Tissue specific module kinase/transcription factor associations
1. tissue-specific-modules-TF.tsv: contains correlations between module odds ratio and transcription factor activities across different tissues

2. tissue-specific-modules-Kinase.tsv: contains correlations between module odds ratio and kinase activities across different tissue

High probability phosphosite pairs derived by different features

1. phos-conf-predictions.tsv: high confidence predictions base on co-phosphorylation

2. expr-conf-predictions.tsv:high confidence predictions base on co-expession

3. expr-phos-conf-predictions.tsv: high confidence predictions base on co-phosphorylation and co-expression

4.all-conf-predictions.tsv: high confidence predictions base on co-phosphorylation and co-expression and co-occurance in data-driven modules
